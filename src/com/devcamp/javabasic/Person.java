package com.devcamp.javabasic;

public class Person {
    //Khai báo thuộc tính
    public String name;//ten
    public int age;//tuoi
    double weight;//can nang
    long salary;//thu nhap
    String[] pets;//thu nuoi trong nha

    //phuong thuc khoi tao khong tham so
    public Person() {
        this.name = "Tran Van Thang";
        this.age = 30;
        this.weight = 60;
        this.salary = 10000000;
        this.pets = new String[] {"cat", "dog"};
    }

    //phuong thuc khoi tao co 2 tham so
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    //phuong thuc khoi tao co 5 tham so
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age);
        // this.name = name;
        // this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
        //this.showInfo();
    }

    //phuong thuc thuong
    public void showInfo() {
        System.out.println(this.name);
        System.out.println(this.age);   
        System.out.println(this.weight);   
        System.out.println(this.salary);   
        System.out.println(this.pets); 
        if (this.pets != null)  {
            for (String pet : pets) {
                System.out.print(pet + ",");
            }
        }
    }

    //phuong thuc overide
    @Override
    public String toString() {
        String result = "Ten: " + this.name;
        result += ", Tuoi: " + this.age;
        result += ", Can nang: " + this.weight;
        result += ", Thu nhap: " + this.salary;
        result += ", Vat nuoi: [";
        if (this.pets != null)  {
            for (String pet : pets) {
                result += pet + ",";
            }
        }
        result += "]";

        return result;
    }
}
