import java.util.ArrayList;

import com.devcamp.javabasic.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Person person1 = new Person("Nguyen Van Nam", 18);
        person1.showInfo();
        // person1.name = "Nguyen Van Nam";
        // person1.age = 18;

        // System.out.println(person1.name);
        // System.out.println(person1.age);

        Person person2 = new Person();
        person2.showInfo();
        // person2.name = "Tran Binh";
        // person2.age = 28;

        // System.out.println(person2.name);
        // System.out.println(person2.age);    

        Person person3 = new Person("Nguyen Hien", 30, 50, 15000000, new String[] {"bird", "pig", "dog"});
        person3.showInfo();

        ArrayList<Person> listPerson = new ArrayList<>();
        listPerson.add(person1);
        listPerson.add(person2);
        listPerson.add(person3);
        listPerson.add(new Person());
        listPerson.add(new Person("Le Thi Hao", 25));
  
        for (int i = 0; i < listPerson.size(); i++) {
            Person person = listPerson.get(i);
            System.out.println(person);
            //person.showInfo();
        }
    }
}
